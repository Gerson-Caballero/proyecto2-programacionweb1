$(document).ready(function () {
    $.ajax({
        url: "http://stackoverflowcgvm.apphb.com/api/recipes",
        type: "GET",
        success: function (data) {
            $(data).each(function (index, element) {
            $(".row").append(`<div class="card" style="width: 18rem; margin:0 auto; margin-top:10px;"><img class="card-img-top" src=${element.ImageUrl} alt="Card image cap"><div class="card-body" style="1margint-top: 10px;"><h1 id="name"class="card-title">${element.Name}</h1><p class="card-text">${element.Servings}Servings</p><p class="card-text">${element.AmountOfTime}min.</p><h3 id="autor" class="card-text">${element.Author}</h3><p class="card-text">${element.Votes}Votos</p><button class="btn btn-info" data-val-id=${element.Id} onclick="detalles(this)">Detalles de receta</button></div></div>`);
            });
        },
        error: function (error) {
            debugger;
        },
    });
});

function detalles(ele){
    $(".container").text("");
    var id=$(ele).attr('data-val-id');
    $.ajax({
        url:`http://stackoverflowcgvm.apphb.com/api/recipes/${id}`,
        type:"GET",
        success: function(data){
            $(data).each(function (index, element) {
                $("#container").append('<header class="main-header"> </header>');
                $(".container").append(`<main> <div class="container"> <section> <h2> ${element.Name} </h2> <div class="dropdown-divider"></div> <div class="row"> <div id="descripcion" class="section-text"> <p> ${element.Description} </p> </div> <table class="tablero table-bordered"> <tr> <td class=""> <h6>Porciones: </h6> </td> <td class=""> <h6>${element.Servings}</h6> </td> </tr> <tr> <td class=""> <h6>Tiempo de preparación: </h6> </td> <td> <h6>${element.AmountOfTime}</h6> </td> </tr> <tr> <td> <h6>Votos: </h6> </td> <td> <h6>${element.Votes}</h6> </td> </tr> <tr> <td> <h6>Por:${element.Author}</h6> </td> </tr> </table> </div> </section> <section> <h2> INGREDIENTES </h2> <div class="dropdown-divider"></div> <p> ${element.RecipeIngredientDetails.IngredientName} ${element.RecipeIngredientDetails.IngredientName} </p> </section> <section> <h2> PASOS </h2> <div class="dropdown-divider"></div> <p> ${element.Steps} </p> </section> </div> </main>`);
            })
        },
        error: function (error) {
            debugger;
        },
    });
}